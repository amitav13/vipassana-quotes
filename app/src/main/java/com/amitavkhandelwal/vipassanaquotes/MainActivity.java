package com.amitavkhandelwal.vipassanaquotes;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.amitavkhandelwal.vipassanaquotes.intro.IntroActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmAsyncTask;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();
    @BindView(R.id.all_quotes_recyclerview) RecyclerView allQuotesRecyclerView;
    private RealmAsyncTask transaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        //  Declare a new thread to do a preference check
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                //  Initialize SharedPreferences
                SharedPreferences getPrefs = PreferenceManager
                        .getDefaultSharedPreferences(getBaseContext());

                //  Create a new boolean and preference and set it to true
//                boolean isFirstStart = getPrefs.getBoolean("firstStart", true);
                boolean isFirstStart = true;
                //  If the activity has never started before...
                if (isFirstStart) {

                    //  Launch app intro
                    Intent intent = new Intent(MainActivity.this, IntroActivity.class);
                    startActivity(intent);

                    //  Make a new preferences editor
                    SharedPreferences.Editor e = getPrefs.edit();

                    //  Edit preference to make it false because we don't want this to run again
                    e.putBoolean("firstStart", false);

                    //  Apply changes
                    e.apply();
                }
            }
        });

        // Start the thread
        t.start();
        transaction = RealmUtils.createRealmDbIfNeeded(this);

        QuotesAdapter allQuotesAdapter = new QuotesAdapter(RealmUtils.getAllQuotes());
        allQuotesRecyclerView.setAdapter(allQuotesAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        allQuotesRecyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public void onStop () {
        if (transaction != null && !transaction.isCancelled()) {
            transaction.cancel();
        }
        super.onStop();
    }
}
