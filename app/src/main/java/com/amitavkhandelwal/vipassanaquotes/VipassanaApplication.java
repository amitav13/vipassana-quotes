package com.amitavkhandelwal.vipassanaquotes;

import android.app.Application;
import android.content.Context;

import io.realm.Realm;
import timber.log.Timber;

public class VipassanaApplication extends Application {

    private static VipassanaApplication context;
    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        Realm.init(this);
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    public static Context getContext() {
        return context;
    }
}
