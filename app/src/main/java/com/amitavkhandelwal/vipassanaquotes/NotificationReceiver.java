package com.amitavkhandelwal.vipassanaquotes;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class NotificationReceiver extends BroadcastReceiver {
    public NotificationReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals(NotificationTimer.NOTIFICATION_ACTION_SHOW_RANDOM_QUOTE)) {
            NotificationTimer.showRandomQuoteNotification(context);
        } else if (action.equals(NotificationTimer.NOTIFICATION_ACTION_DISMISS)) {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(NotificationTimer.NOTIFICATION_ID);
        }
    }
}
