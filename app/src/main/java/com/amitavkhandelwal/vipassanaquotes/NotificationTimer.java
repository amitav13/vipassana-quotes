package com.amitavkhandelwal.vipassanaquotes;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;

import java.util.Calendar;
import java.util.List;
import java.util.Random;

public class NotificationTimer {

    public static final int NOTIFICATION_ID = 1; //kept constant to update older notifications instead of spawning new ones
    public static final String NOTIFICATION_ACTION_SHOW_RANDOM_QUOTE = "NOTIFICATION_ACTION_SHOW_RANDOM_QUOTE";
    public static final String NOTIFICATION_ACTION_DISMISS = "NOTIFICATION_ACTION_DISMISS";
    public static final String NOTIFICATION_ACTION_FAVOURITE = "NOTIFICATION_ACTION_FAVOURITE";
    private static final int REQUEST_CODE = 1;

    public static void showNotification(@NonNull Context context, @NonNull Quote quote) {
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationCompatBuilder = new NotificationCompat.Builder(context);

        Intent dismissIntent = new Intent(context, NotificationReceiver.class);
        dismissIntent.setAction(NOTIFICATION_ACTION_DISMISS);
        PendingIntent dismissPendingIntent = PendingIntent.getBroadcast(context, REQUEST_CODE, dismissIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = notificationCompatBuilder
                .setAutoCancel(true)
                .setContentTitle(context.getString(R.string.notification_title))
                .setContentText(quote.getQuoteDescription())
                //TODO fix icon
                .setSmallIcon(R.drawable.ic_buddha_amber_256x256)
                .setDefaults(Notification.DEFAULT_ALL)
                .setStyle(new NotificationCompat.BigTextStyle())
                .addAction(R.drawable.ic_clear_black_24dp, context.getString(R.string.notification_action_dismiss), dismissPendingIntent)
                .build();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, notification);
    }

    public static void showRandomQuoteNotification(@NonNull Context context) {
        List<Quote> quotes = RealmUtils.getAllQuotes();
        Random randomGenerator = new Random();
        int index = randomGenerator.nextInt(quotes.size());
        Quote randomQuote = quotes.get(index);
        showNotification(context, randomQuote);
    }

    public static void setNotificationTime(@NonNull Context context, int hour, int minute) {
        if (hour < 0 || hour > 23 || minute < 0 || minute > 59) {
            throw new RuntimeException("Notification time parameters are illegal.");
        }
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 0);
        Intent intent = new Intent(context, NotificationReceiver.class);
        intent.setAction(NOTIFICATION_ACTION_SHOW_RANDOM_QUOTE);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        am.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
    }
}
