package com.amitavkhandelwal.vipassanaquotes.intro;


import android.animation.ObjectAnimator;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.amitavkhandelwal.vipassanaquotes.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Fragment used as a step in the introduction of the app to allow for the user to pick a notification
 * time.
 */
public class NotificationTimePickerFragment extends Fragment implements SeekBar.OnSeekBarChangeListener {

    @BindView(R.id.progressBar) ProgressBar progressBar;
    @BindView(R.id.seekBar) SeekBar seekBar;
    @BindView(R.id.time_text) TextView timeText;

    public NotificationTimePickerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.notification_time_picker_fragment, container, false);
        ButterKnife.bind(this, rootView);

        progressBar.setProgress(18);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressBar.setProgressTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
        } else {
            progressBar.getProgressDrawable().setColorFilter(
                    getResources().getColor(R.color.white), PorterDuff.Mode.MULTIPLY);
        }
        seekBar.setOnSeekBarChangeListener(this);

        return rootView;
    }

    public static String generateTimeText(int progress) {
        if (progress <= 0 || progress > 23) {
            return "12 AM";
        }
        String number = (progress > 12) ? Integer.toString(progress - 12) : Integer.toString(progress);
        String postfix = (progress >= 12) ? "PM" : "AM";
        return number + " " + postfix;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        ObjectAnimator animation = ObjectAnimator.ofInt (progressBar, "progress", seekBar.getProgress(), progress);
        animation.setDuration(5000); //in milliseconds
        animation.setInterpolator(new DecelerateInterpolator());
        animation.start();

        timeText.setText(generateTimeText(progress));
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        progressBar.clearAnimation();
    }
}
