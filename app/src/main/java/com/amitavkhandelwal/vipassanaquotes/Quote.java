package com.amitavkhandelwal.vipassanaquotes;

import io.realm.RealmObject;

public class Quote extends RealmObject {
    //String quoteTitle;
    private String quoteDescription;
    private boolean isFavourited;

    public String getQuoteDescription() {
        return quoteDescription;
    }

    public void setQuoteDescription(String quoteDescription) {
        this.quoteDescription = quoteDescription;
    }

    public boolean isFavourited() {
        return isFavourited;
    }

    public void setFavourited(boolean favourited) {
        isFavourited = favourited;
    }
}
