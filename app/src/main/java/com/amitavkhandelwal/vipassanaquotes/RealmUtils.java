package com.amitavkhandelwal.vipassanaquotes;

import android.content.Context;
import android.support.annotation.Nullable;

import io.realm.Realm;
import io.realm.RealmAsyncTask;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import timber.log.Timber;


public class RealmUtils {

    public static RealmResults<Quote> getAllQuotes() {
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<Quote> query = realm.where(Quote.class);
        return query.findAll();
    }

    @Nullable
    public static RealmAsyncTask createRealmDbIfNeeded(Context context) {
        Realm realm = Realm.getDefaultInstance();
        RealmAsyncTask transaction = null;
        if (realm.isEmpty()) {
            //initialize complete database of quotes
            final String[] quotes = context.getResources().getStringArray(R.array.quotes);
            transaction = realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    for (String quoteText : quotes) {
                        Quote quote = realm.createObject(Quote.class);
                        quote.setFavourited(false);
                        quote.setQuoteDescription(quoteText);
                    }
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    Timber.d("Realm database initiated successfully");
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    //TODO handle error
                    Timber.e("Error initiating Realm database");
                    error.printStackTrace();
                }
            });
        }
        return transaction;
    }
}
