package com.amitavkhandelwal.vipassanaquotes;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QuotesAdapter extends RecyclerView.Adapter<QuotesAdapter.QuoteViewHolder> {

    private List<Quote> quotesList;

    public QuotesAdapter(List<Quote> quotesList) {
        this.quotesList = quotesList;
    }

    @Override
    public QuoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView view = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.quote_list_item, parent, false);
        return new QuoteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(QuoteViewHolder holder, int position) {
        Quote quote = quotesList.get(position);
        holder.quoteText.setText(quote.getQuoteDescription());
        holder.quote = quote;
    }

    @Override
    public int getItemCount() {
        return quotesList.size();
    }

    public class QuoteViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private CardView cardView;
        private Quote quote;
        @BindView(R.id.quote_text) TextView quoteText;

        public QuoteViewHolder(View view) {
            super(view);
            this.cardView = (CardView) view;
            ButterKnife.bind(this, cardView);
            this.cardView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            //TODO add quote to favourites
            NotificationTimer.showNotification(VipassanaApplication.getContext(), quote);
        }
    }
}
