package com.amitavkhandelwal.vipassanaquotes;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;
import android.widget.FrameLayout;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import timber.log.Timber;

import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class TestNotification {

    private UiDevice device;
    private Context context;

    @Before
    public void setUp() throws Exception {
        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        context = InstrumentationRegistry.getTargetContext();
    }

    @Test
    public void testIfNotificationShows() throws Exception {
        Quote testQuote = new Quote();
        String testQuoteDescription = "This is a test quote.";
        testQuote.setQuoteDescription(testQuoteDescription);
        NotificationTimer.showNotification(context, testQuote);
        //TODO get the uiselector to work.
//        device.openNotification();
//
//        UiSelector selector = new UiSelector();
//        selector.textMatches("asas");
//        selector.className(Notification.class);
//        UiObject notification = device.findObject(selector);
//        Timber.d(notification.getClassName());
//        Timber.d(notification.getContentDescription());
//        Timber.d(notification.getPackageName());
//        Timber.d(notification.getChildCount() + "");
//        assertThat(notification.exists(), is(true));
//
//        UiObject dismissButton = notification.getChild(new UiSelector().textMatches("DISMISS"));
//        assertThat(dismissButton.exists(), is(true));
//
//        dismissButton.click();
//        device.waitForIdle();
//        notification.waitUntilGone(3000);
//        device.openNotification();
//        notification = device.findObject(selector);
//        assertThat(notification.exists(), is(false));
        device.openNotification();
        device.wait(Until.hasObject(By.pkg("com.android.systemui")), 10000);

    /*
     * access Notification Center through resource id, package name, class name.
     * if you want to check resource id, package name or class name of the specific view in the screen,
     * run 'uiautomatorviewer' from command.
     */
        UiSelector notificationStackScroller = new UiSelector().packageName("com.android.systemui")
                .className(FrameLayout.class)
                .resourceId(
//                        "com.android.systemui:id/notification_stack_scroller"
                "com.android.systemui:id/expanded"
                );
        UiObject notificationStackScrollerUiObject = device.findObject(notificationStackScroller);
        assertTrue(notificationStackScrollerUiObject.exists());

    /*
     * access top notification in the center through parent
     */
        UiObject notification = notificationStackScrollerUiObject.getChild(new UiSelector().index(0));
        assertTrue(notification.exists());
        Timber.d(notification.getClassName());
        Timber.d(notification.getContentDescription());
        Timber.d(notification.getPackageName());
        notification.click();
    }
}
