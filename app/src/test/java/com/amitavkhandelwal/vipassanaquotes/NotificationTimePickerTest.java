package com.amitavkhandelwal.vipassanaquotes;

import com.amitavkhandelwal.vipassanaquotes.intro.NotificationTimePickerFragment;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NotificationTimePickerTest{

    @Test
    public void generateTimeTextCorrectly() {
        assertEquals("12 AM", NotificationTimePickerFragment.generateTimeText(0));
        assertEquals("1 AM", NotificationTimePickerFragment.generateTimeText(1));
        assertEquals("12 PM", NotificationTimePickerFragment.generateTimeText(12));
        assertEquals("1 PM", NotificationTimePickerFragment.generateTimeText(13));
        assertEquals("11 PM", NotificationTimePickerFragment.generateTimeText(23));

    }
}
